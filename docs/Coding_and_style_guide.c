#pragma once // using pragma to guard agains multiple inclusions. Put up with it.

#include <first_include_std.h>
#include <like_this.h>
// one line between the includes
#include "then_include_local.h"
#include "like_this.h"
// at least two lines between includes and first piece of code

// macros go here if needed
#ifndef ZC_MACRO // always guard macros
#define ZC_MACRO value // macros always begin with identification code, aka ZC_HEADER_NAME_MACRO_NAME
#endif
// at least two lines between macros and first piece of code

int function_name(char *pointer, int argument) // a function returns int codes, not values.
{
	// return 0 if everything went well. Other return codes should be declared
	// in the given project's documentation.
	// these are always positive values, for consistency reasons.
	
	// the above rule is only to be broken when a function needs to return a value for ease of use.
	// a specific example would be a sigmoid squash(), sin(), cos(), tangent(), or a clamp() function.
	
	// Code
	
	// Do note that comments are lines like this.
	/* This kind of comment belongs OUTSIDE of code, NEVER inside of any function */
	// The reason for this, is to be able to comment out entire functions seamlessly, when necessary.
	
	
	// Some spacing, 1-2 lines
	return 0;
}
// 2 lines separate functions, by standard.
// Comment on the function here
int another_function() // Other comments (especially arguments) go here
{
	// Other comments (especially on code) go here
	// Code
	
	return 0;
}
