#pragma once // pragma-t használunk include guard-nak, ha tetszik, ha nem.

#include <először_include_sztenderd.h>
#include <például_így.h>
// egy sor az include-ok között
#include "aztán_include_lokálisakat.h"
#include "mint_ez.h"
// legalább két sor az include-ok, és az első kóddarabka közt

// makrókat itt definiáljunk
#ifndef ZC_MACRO // mindig, minden makrót védeni kell
#define ZC_MACRO value // makrók mindig ID-vel kezdődnek, lásd: ZC_HEADER_NÉV_MACRO_NÉV
#endif
// legalább két sor a makrók, és az első kóddarabka közt

int function_name(char *pointer, int argument) // a funkciók int kódokat dobnak vissza, nem értékeket.
{
	// return 0 ha minden rendben ment. Egyéb return kódok az adott projektre
	// specifikusan legyenek megállapítva a dokumentációban.
	// Ezek mindig legyenek pozitív értékek, hogy konzisztensek legyünk.
	
	// A fentebbi szabályt csak akkor szeghetjük meg, ha egy funkciónak vissza kell adnia egy értéket
	// hogy könnyebben használható legyen.
	// jó példa erre a sigmoid_squash(), sin(), cos(), tan(), vagy a clamp() funkció.
	
	// Kód
	
	// A kommentek ilyen sorok legyenek
	/* Ez a fajta komment a funkciókon / kódon kívülre való, SOHA nem azon belülre. */
	// Ennek az az oka, hogy egész funkciókat ki lehessen kommentelni ügyeskedés nélkül, ha arra lenne szükség.
	
	
	// Általános távolságtartás, 1-2 sor
	return 0;
}
// 2 sor választja el a funkciókat
// Egy adott funkcióra vonatkozó komment lehet előtte
int another_function() // Egyéb komment (főleg argumentumok) ide
{
	// Egyéb komment (főleg kódbázisra vonatkozó) ide
	// Kód
	
	return 0;
}
