#pragma once
#include <math.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062L
#endif

#ifndef M_E
#define M_E 2.71828182845904523536028747135266249775724709369995L
#endif

/* MATH */

double to_degrees(double rad)
{
	return rad * (180.0 / M_PI);
}


double to_radians(double deg)
{
	return deg * (M_PI / 180.0);
}


double clamp(double in, double min, double max)
{
	if (in > max)
	{
		return max;
	}
	else if (in < min)
	{
		return min;
	}
	else
	{
		return in;
	}
}


double sigmoid(double in)
{
	return (1 / (1 + pow(M_E, (in * -1))));
}


int sgn(double val)
{
	return val < 0 ? -1 : (val == 0) ? 0 : 1;
}
