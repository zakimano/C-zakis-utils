#pragma once

#include <stdlib.h>
#include <stdio.h>

#include "zc_string/zc_auto-string.h"

#ifndef ZC_READ_FILE_BUFFER_LIMIT
#define ZC_READ_FILE_BUFFER_LIMIT 2048
#endif



/*
 * Header file for the function 'readFile'
 */

int read_file(FILE *fIn, astr *astr_out)
{
	#ifdef DEBUG
	unsigned int i = 0;
	#endif
	
	// create buffer
	char buffer[ZC_READ_FILE_BUFFER_LIMIT];
	int j;
	for (j = 0; j < ZC_READ_FILE_BUFFER_LIMIT; j++)
	{
		buffer[j] = '\0';
	}
	
	// return val
	int rtval;
	
	do
	{
		fgets(buffer, ZC_READ_FILE_BUFFER_LIMIT - 1, fIn);
		
		rtval = astr_append(astr_out, &buffer); // try adding buffer's content
		if (rtval)
			return rtval; // return if something went wrong
		
		#ifdef DEBUG
		i++;
		printf("zc_read_file: iter: %d\n", i); // debug print
		#endif
		
	} while (!feof(fIn));
	
	// signal success
	
	return 0;
}

//fscanf(fIn, "%s", buffer); formatting-terminated, formatted input!

//fread(buffer, 20, 1, fIn); limit-terminated

//fgets(buffer, 100, fIn); newline-terminated, finite.

//fgetc(fIn); one character
