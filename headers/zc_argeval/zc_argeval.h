#pragma once

#include <stdlib.h>
#include <argp.h>
#include <stdbool.h>

/*
 * Header file for argument parsing, and general Argp stuff
 *
 * Docs:
 * https://www.gnu.org/software/libc/manual/html_node/Argp.html#Argp
 * https://stackoverflow.com/questions/9642732/parsing-command-line-arguments#24479532
 *
 * Maybe worth to look at:
 * https://github.com/jamesderlin/dropt
*/

#pragma GCC diagnostic push // 3, 2, 1...
#pragma GCC diagnostic ignored "-Wmissing-field-initializers" // Stop these warnings from appearing
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"


// Define some basic info about the program
const char *argp_program_version = "programName programVersion";
const char *argp_program_bug_address = "<your@email.address>";
static char doc[] = "Your program description.";
static char args_doc[] = "[NON-OPTION-ARG]";

// Define help for options
static struct argp_option options[] = {
	{ "option1", 'j', 0, 0, "option1 aka j"},
	{ "option2", 'k', 0, 0, "*description of option2*"},
	{ "option3", 'l', 0, 0, "sajt."},
	{ 0 }
};

// Define the 'arguments' struct
struct arguments {
	enum { DEFAULT_OPT, OPT1, OPT2 } mode;
	bool aBoolSettingForYou;
};

// The actual parsing happens here.
// !! Make sure this aligns with the help section !!
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;
	switch (key)
	{
		case 'j': arguments->mode = OPT1; break;
		case 'k': arguments->mode = OPT2; break;
		case 'l': arguments->aBoolSettingForYou = true; break;
		case ARGP_KEY_ARG: return 0;
		default: return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };

#pragma GCC diagnostic pop // Let warnings appear again

// Okay, we are done.

/* TO INCLUDE IN MAIN
int main(int argc, char *argv[])
{
	struct arguments arguments;
	
	arguments.mode = CHARACTER_MODE;
	arguments.isCaseInsensitive = false;
	
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	
	// ...
}
*/
