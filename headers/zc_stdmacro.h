#pragma once
#include <stdlib.h>
#include <stdio.h>


#define STDERR_PRINT_AND_EXIT(MSG) \
do \
{ \
	fprintf(stderr, "%s", MSG); \
	exit(1); \
} while (0)


#define STDERR_PRINT(MSG) \
do \
{ \
	fprintf(stderr, "%s", MSG); \
} while (0)