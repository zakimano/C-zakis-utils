#pragma once
#include <stdlib.h>
#include <time.h>

/* TIME */

// sleep functions

#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else
#include <unistd.h> // for usleep
#endif

void sleep_ms(int milliseconds) // cross-platform sleep function
{
	#ifdef WIN32
	Sleep(milliseconds);
	
	#elif _POSIX_C_SOURCE >= 199309L
	struct timespec ts;
	ts.tv_sec = milliseconds / 1000;
	ts.tv_nsec = (milliseconds % 1000) * 1000000;
	nanosleep(&ts, NULL);
	
	#else
	usleep(milliseconds * 1000);
	
	#endif
}

void sleep_s(int seconds)
{
	#ifdef WIN32
	Sleep(seconds * 1000);
	
	#elif _POSIX_C_SOURCE >= 199309L
	struct timespec ts;
	ts.tv_sec = seconds;
	ts.tv_nsec = 0;
	nanosleep(&ts, NULL);
	
	#else
	usleep(seconds * 1000000);
	
	#endif
}
