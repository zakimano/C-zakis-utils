#pragma once

#include <string.h>

/*
 * Header file for text handling
*/



#ifndef ZC_STRL_LENGTH
#define ZC_STRL_LENGTH 1000
#endif

// TODO FIX

typedef struct indexed_linked_list_string // The type used from the outside
{
	struct strl_string* first;
	int num_of_indexes;
	struct strl_string* last;
} illstr;


static typedef struct strl_string // local type, used for actually storing the strings
{
	struct strl_string* prev;
	char line[ZC_STRL_LENGTH];
	int length;
	int index;
	struct strl_string* next;
} strl;


static strl* strl_create_block(int index)
{
	strl *newStrL = malloc(sizeof(strl));
	if (newStrL != NULL)
	{
		newStrL->prev = NULL;
		memcpy(newStrL->line, "\0", 1);
		newStrL->length = 0;
		newStrL->index = index;
		newStrL->next = NULL;
	}
	return newStrL;
}


static void strl_remove_block(strl *toRemove)
{
	strl *prevBlock = NULL;
	strl *nextBlock = NULL;
	
	nextBlock = toRemove->next;
	prevBlock = toRemove->prev;
	
	if (prevBlock != NULL)
		prevBlock->next = nextBlock;
	
	if (nextBlock != NULL)
		nextBlock->prev = prevBlock;
	
	free(toRemove);
}


static strl* strl_add_block(strl *strlListLast)
{
	strl *newStrL = strl_create_block();
	if (newStrL != NULL)
	{
		if (strlListLast != NULL && strlListLast->next == NULL)
		{
			strlListLast->next = newStrL;
			newStrL->prev = strlListLast;
		}
	}
	return newStrL;
}


static int strl_delete_string_list(strl *toDelete)
{
	while (toDelete != NULL)
	{
		if (toDelete->prev != NULL)
		{
			strl_remove_block(toDelete->prev);
		}
		else if (toDelete->next != NULL)
		{
			strl_remove_block(toDelete->next);
		}
		else
		{
			strl_remove_block(toDelete);
		}
	}
	return 0;
}


static int strl_write_to_bock(strl *strlTargetBlock, char str[]) // TODO: complete write
{
	int sa, len;
	sa = ZC_STRL_LENGTH - strlen(strlTargetBlock.line);
	len = strlen(str);
	
	if (sa > len)
	{
		
	}
	else
	{
		return 1;
	}
	return 0;
}


static int strl_read_from_block(strl *strlTargetBlock, char *str) // TODO: complete read
{
	if (/* condition */)
	{
		
	}
	else
	{
		
		return 1;
	}
	return 0;
}


//

strl* strl_append(strl *strlListLast, char toAppend[]) // TODO: complete / fix append
{
	
}
