#pragma once
#include <stdio.h>


/* COLOR SUPPORT */
/* please look at https://en.wikipedia.org/wiki/ANSI_escape_code#Colors for extended support */

// 2 x 8 colors:
#define ANSI_COLOR_BLACK \033[38;5;0m
#define ANSI_COLOR_RED \033[38;5;1m
#define ANSI_COLOR_GREEN \033[38;5;2m
#define ANSI_COLOR_YELLOW \033[38;5;3m
#define ANSI_COLOR_BLUE \033[38;5;4m
#define ANSI_COLOR_PURPLE \033[38;5;5m
#define ANSI_COLOR_CYAN \033[38;5;6m
#define ANSI_COLOR_GREY \033[38;5;7m
#define ANSI_COLOR_GREY2 \033[38;5;8m
#define ANSI_COLOR_RED2 \033[38;5;9m
#define ANSI_COLOR_GREEN2 \033[38;5;10m
#define ANSI_COLOR_YELLOW2 \033[38;5;11m
#define ANSI_COLOR_BLUE2 \033[38;5;12m
#define ANSI_COLOR_PURPLE2 \033[38;5;13m
#define ANSI_COLOR_CYAN2 \033[38;5;14m
#define ANSI_COLOR_WHITE \033[38;5;15m

// 256 colors:
#define ANSI_COLOR_CUSTOM(CODE) \033[38;5;CODEm

// This should reset everything else, not just color!
#define ANSI_COLOR_RESET \033[0m

#define AC_PRINT(STR, CC) printf("\033[38;5;%dm%s\033[0m", CC, STR);