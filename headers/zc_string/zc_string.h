#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>


/* STRINGS */

int str_find_replace(char *haystack, char **stackOfHay, char *needle, char *replace, int limit)
{
	char *hay = NULL;
	hay = (char *)calloc(limit, sizeof(char));
	if (hay == NULL) //we good? we good.
		return 1;
	
	memcpy(hay, haystack, strlen(haystack));
	
	char *npt, *cpt, *apt;
	npt = cpt = apt = NULL;
	int len, i;
	
	i = strlen(haystack) + 1;
	if (i >= limit) //are you an idiot?
		return 1;
	
	while (strstr(hay, needle) != NULL)
	{
		npt = strstr(hay, needle);
		
		cpt = npt + strlen(needle);
		len = strlen(cpt);
		//copy stuff after continuation pointer to keep them for later
		apt = (char *)calloc(len + 1, sizeof(char));
		memcpy(apt, cpt, len);
		//set string after needle pointer to zeros
		memset(npt, '\0', len + strlen(needle));
		
		i = strlen(hay) + strlen(apt) + strlen(replace);
		if (i >= limit)
			return 1;
		
		//append REPLACE
		strcat(npt, replace);
		//mod. cpt to be on end of appended text
		cpt = npt + strlen(npt);
		
		//append the rest
		strcat(cpt, apt);
		
		//free stuff
		free(apt);
	}
	
	*stackOfHay = hay;
	
	return 0;
}


int str_find_omit(char *haystack, char **stackOfHay, char front, char end, int limit)
{
	char *hay = NULL;
	hay = (char *)calloc(limit, sizeof(char));
	if (hay == NULL) //we good? we good.
	return 1;

	memcpy(hay, haystack, strlen(haystack));
	
	char *npt, *cpt, *apt;
	npt = cpt = apt = NULL;
	int len, i;
	
	i = strlen(haystack) + 1;
	if (i >= limit) //are you an idiot?
		return 1;
	
	while (strchr(hay, front))
	{
		npt = strchr(hay, front);
		cpt = strchr(hay, end);
		len = strlen(cpt);
		//copy stuff after continuation pointer to keep them for later
		apt = (char *)calloc(len + 1, sizeof(char));
		memcpy(apt, cpt + 1, len);
		//set string after needle pointer to zeros
		memset(npt, '\0', len);
		
		i = strlen(hay) + strlen(apt);
		if (i >= limit)
			return 1;
		
		//mod. cpt to be on end of text
		cpt = npt + strlen(npt);
		
		//append the rest
		strcat(cpt, apt);
		
		//free stuff
		free(apt);
	}
	
	*stackOfHay = hay;
	
	return 0;
}