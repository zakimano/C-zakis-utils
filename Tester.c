
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/zc_string/zc_auto_string.h"
#include "headers/zc_argeval/zc_argeval.h"

int main(int argc, char const *argv[])
{
	struct arguments arguments;
	
	arguments.mode = DEFAULT_OPT;
	arguments.aBoolSettingForYou = false;
	
	#pragma GCC diagnostic push // 3, 2, 1...
	#pragma GCC diagnostic ignored "-Wincompatible-pointer-types" // Stop this warning from appearing
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	#pragma GCC diagnostic pop // Let the warnings take over again.
	
	astr a;
	astr_init(&a);
	
	char b[] = "If the pattern ends with a slash, it is removed for the purpose of the following description, but it would only find a match with a directory. In other words, foo/ will match a directory foo and paths underneath it, but will not match a regular file or a symbolic link foo (this is consistent with the way how pathspec works in general in Git).\0";
	
	astr_append(&a, b);
	
	printf("%s\n", a.start);
	
	astr_delete(&a);
	
	return 0;
}
